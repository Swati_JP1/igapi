package com.IGDemo.Constants;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.IGDemo.Common.RestUtilities;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.ResponseSpecification;

public class APIMock {

	public static int verifyStatusCode(JsonPath js) {
		return js.get("status");
	}

	public static String verifyHeader(Response resp, String headerName) {
		return resp.getHeader(headerName);
	}

	public static String verifyMessageData(JsonPath js, String path) {
		return js.get(path);
	}

	public static String verifyArrayResponseData(JsonPath js, String path) {
		String jsonArray = "";
        try {
        	 jsonArray = js.getString(path);
        }
        catch(Exception e){
        	 e.getMessage();
        }
		return jsonArray.toString();
	}
	
	

	public static void verifyOutputStatus(boolean bool, String Message) {
		// TODO Auto-generated method stub
		if(bool) {
			System.out.println("Pass - " + Message );
		}
		else {
			System.out.println("Fail - " + Message);
		}
	}

	
}
