package com.IGDemo.Tests;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.IGDemo.Common.RestUtilities;
import com.IGDemo.Constants.APIMock;
import com.IGDemo.Constants.EndPoints;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

public class APIMockTests {
    RequestSpecification requestSpec;
    ResponseSpecification responseSpec;
    
	@BeforeClass
	public void setUp() {
		requestSpec = RestUtilities.getRequestSpecBuilder();
		responseSpec = RestUtilities.getResponseSpecBuilder();
	}
	
	@Test(description = "Verify API Test assertions.")
	public void verifyAPIResponse() {
		Response response = given().spec(requestSpec).when().get(EndPoints.ENDPOINT).then().spec(responseSpec).extract().response();
		JsonPath jsResponseData = RestUtilities.getJsonPath(response);
		System.out.println(jsResponseData.get().toString());
		APIMock.verifyOutputStatus(APIMock.verifyStatusCode(jsResponseData) == 200, "Response Status code Verified");
		APIMock.verifyOutputStatus(APIMock.verifyHeader(response,"Content-Type").equals("application/json; charset=UTF-8"), "Response Header Verified");
		APIMock.verifyOutputStatus(APIMock.verifyArrayResponseData(jsResponseData, "status").equals("200"),"Response Body Status code Verified");
		APIMock.verifyOutputStatus(APIMock.verifyMessageData(jsResponseData, "message").equals("data retrieved successful"), "Response body Message Verified");
		APIMock.verifyOutputStatus(APIMock.verifyArrayResponseData(jsResponseData,"employeeData.role[0]").equals("QA Automation Developer"), "Response body Role Verified");
		APIMock.verifyOutputStatus(APIMock.verifyArrayResponseData(jsResponseData, "employeeData.age[0]").equals("25"), "Response body Age Verified");
		APIMock.verifyOutputStatus(APIMock.verifyArrayResponseData(jsResponseData,"employeeData.dob[0]").equals("25-02-1994"), "Response body DOB Verified");
		APIMock.verifyOutputStatus(APIMock.verifyArrayResponseData(jsResponseData, "employeeData.company[0]").equals("ABC Infotech"), "Response body Company Verification Failed");
		
	}
}
