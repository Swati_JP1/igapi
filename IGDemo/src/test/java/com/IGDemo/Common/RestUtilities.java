package com.IGDemo.Common;

import static io.restassured.RestAssured.given;

import com.IGDemo.Constants.Path;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;

public class RestUtilities {
	public static String ENDPOINT;
	public static RequestSpecBuilder REQUEST_BUILDER;
    public static RequestSpecification REQUEST_SPEC;
    public static ResponseSpecBuilder RESPONSE_BUILDER;
    public static ResponseSpecification RESPONSE_SPEC;
    
    public static void setEndpoint(String endpoint) {
   	 ENDPOINT = endpoint;
    }
    
    public static RequestSpecification getRequestSpecBuilder() {
   	 REQUEST_BUILDER = new RequestSpecBuilder();
   	 REQUEST_BUILDER.setBaseUri(Path.BASE_URI);
   	 REQUEST_SPEC = REQUEST_BUILDER.build();
   	 return REQUEST_SPEC;
    }
    
    public static ResponseSpecification getResponseSpecBuilder() {
   	 RESPONSE_BUILDER = new ResponseSpecBuilder();
   	 RESPONSE_SPEC = RESPONSE_BUILDER.build();
   	 return RESPONSE_SPEC;
    }
    
    public static Response getResponse(RequestSpecification reqSpec, String Type) {
   	 REQUEST_SPEC.spec(reqSpec);
   	 Response response = null;
   	 if(Type.equalsIgnoreCase("get")) {
   		 response = given().spec(REQUEST_SPEC).get(ENDPOINT);
   	 }
   	 else {
   		 System.out.println("Type is not supported");
   	 }
   	 response.then().spec(RESPONSE_SPEC);
   	 return response;
    }
    
    
    public static JsonPath getJsonPath(Response resp) {
   	 String jsonString = resp.asString();
   	 return new JsonPath(jsonString);
    }
    
}
